ffmpeg -f avfoundation -video_size 1280x720 -framerate 30 -i "0" -vcodec libx264 -preset veryfast -f hls -hls_time 1 -hls_list_size 4 -hls_allow_cache 0 stream.m3u8

::List webcam format available
ffmpeg -f avfoundation -list_devices true -i ""

::video sizes
640x480
1280x720

::With time overlay and live manifest
ffmpeg -f avfoundation -filter_complex "drawtext=fontsize=90:fontfile=/Library/Fonts/Arial.ttf: text='%{localtime\:%H %M %S}': x=100 : y=50 : box=1" -video_size 1280x720 -framerate 30 -i "0" -vcodec libx264 -preset veryfast -f hls -hls_time 1 -hls_list_size 4 -hls_allow_cache 0 stream.m3u8

::Webcam stream to RTMP
ffmpeg -f avfoundation -filter_complex "drawtext=fontsize=90:fontfile=/Library/Fonts/Arial.ttf: text='%{localtime\:%H %M %S}': x=100 : y=50 : box=1" -video_size 1280x720 -framerate 30 -i "0" -vcodec libx264 -preset ultrafast -f flv rtmp://127.0.0.1/show/stream

::Pull and launch docker machine
docker pull stephanecloudflared/cloudflared
git clone https://gitlab.com/stephane5/streambox
docker run -d -p 1935:1935 -v $PWD/www:/var/www/html stephanecloudflare/cloudflared
